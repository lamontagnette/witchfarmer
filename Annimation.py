import pygame

class Annimation:

    def __init__(self,x,y,list_image,rate):
        self.x = x
        self.y = y
        self.list_image = list_image
        self.rate = rate
        self.index = 0

    def print(self, screen, total_delta_x, total_delta_y, cpt):
        if cpt%self.rate == 0 :
            if self.index < len(self.list_image) :
                screen.blit(self.list_image[self.index], (self.x + total_delta_x, self.y + total_delta_y))
                self.index += 1