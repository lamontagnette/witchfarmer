import pygame
from time import time
from random import randint
from math import sqrt
from Player import *
from Ui import *
from Weapon import *
from Ennemie import *
from Animal import *
from Map import *
from Level import *
from Ressource import *
from utils import *
from cte import *



def main():
    # Initialisation de la fenêtre d'affichage
    pygame.init()
    
    screen = pygame.display.set_mode(SCREEN_SIZE)
    #screen = pygame.display.set_mode(SCREEN_SIZE,pygame.FULLSCREEN)

    # Remplissage de l'arrière-plan
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((0, 204, 255))
 
    # Fonts 
    font_items = pygame.font.Font('slabo.ttf', 14) 

    # Afficher le tout dans la fenêtre
    screen.blit(background, (0, 0))
    pygame.display.flip()

    # Var init
    delta_y = 0
    delta_x = 0
    mouse_position = 0
    #dict_input = {pygame.K_UP:False,pygame.K_DOWN:False,pygame.K_LEFT:False,pygame.K_RIGHT:False,"MOUSE_CLICK":False}
    dict_input = {pygame.K_w:False,pygame.K_s:False,pygame.K_a:False,pygame.K_d:False,"MOUSE_CLICK":False}
    list_collision = []

    # Map init
    map_size = (30,30)
    game_map = Map(map_size)
    for i in range(game_map.size[0]):
        for j in range(game_map.size[1]):
            if game_map.bitmap[i][j] == 'e' or game_map.bitmap[i][j] == 'ed' :
                list_collision.append((i*64 + 9,j*64,(46,64)))
            #screen.blit(game_map.dict_block[game_map.bitmap[i][j]], (i*64, j*64))

    # Level init
    level = Level(1,{'arbuste':20,'pierre':10,'arbre':10})
    list_collision = level.init_res_instances(map_size,list_collision)
    
    # Weapon init
    weapon = Weapon("basic")


    # Player init
    x = randint(0,map_size[0]*64-64)
    y = randint(0,map_size[1]*64-64)
    while chevoch_list((x+14,y+18,(32,42)),list_collision):
        x = randint(0,map_size[0]*64-64)
        y = randint(0,map_size[1]*64-64)
    player = Player(x,y,weapon)

    # Ennemie init
    x = randint(0,map_size[0]*64-64)
    y = randint(0,map_size[1]*64-64)
    while chevoch_list((x,y,(64,64)),list_collision):
        x = randint(0,map_size[0]*64-64)
        y = randint(0,map_size[1]*64-64)
    slime = Ennemie(x,y)

    # Hen init
    hen_list = []
    for i in range(20):
        x = randint(0,map_size[0]*64-64) + 16
        y = randint(0,map_size[1]*64-64) + 16
        while chevoch_list((x,y,(64,64)),list_collision):
            x = randint(0,map_size[0]*64-64) + 16
            y = randint(0,map_size[0]*64-64) + 16
        hen_list.append(Animal(x,y,"egg"))
    
    # UI init
    ui = Ui(player)

    # Init list items on floor

    list_items = []

    # Center Player and mv Obj

    total_delta_x =  SCREEN_SIZE[0]//2 - 32 - player.x
    total_delta_y =  SCREEN_SIZE[1]//2 - 32 - player.y

    player.x = SCREEN_SIZE[0]//2 - 32
    player.y = SCREEN_SIZE[1]//2 - 32

    clock = pygame.time.Clock()
    
    time_cpt = 0
    action_done = False
    on_action_obj = None

    # Boucle d'évènements
    game_continue = True
    while game_continue:

        time_cpt += 1

        # GESTION DU TEMPS
        timedelta = clock.tick(60)
        timedelta /= 1000

        # GESTION DES EVENEMENTS
        for event in pygame.event.get():
                if event.type == pygame.QUIT or ( event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                        game_continue = False

                elif event.type == pygame.KEYDOWN:
                    dict_input[event.key] = True

                elif event.type == pygame.KEYUP:
                    dict_input[event.key] = False
                    if event.key == pygame.K_w or event.key == pygame.K_s :
                        delta_y = 0
                    if event.key == pygame.K_a or event.key == pygame.K_d :
                        delta_x = 0

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        dict_input["MOUSE_CLICK"] = True
                    elif event.button == 4:
                        ui.change_selected(1)
                    elif event.button == 5:
                        ui.change_selected(-1)
                    
                elif event.type == pygame.MOUSEBUTTONUP:
                    if event.button == 1:
                        dict_input["MOUSE_CLICK"] = False
                        action_done = False
                    
                elif event.type == pygame.MOUSEMOTION:
                    mouse_position = event.pos
                    # Chg de repère
                    m_p = (mouse_position[0] - SCREEN_SIZE[0]//2, mouse_position[1] - SCREEN_SIZE[1]//2 )
                    d = sqrt(m_p[0]**2 + m_p[1]**2) + 0.001

                    m_p = (m_p[0]/d,m_p[1]/d)

                    #print("mouse at (%d, %d)" % mouse_position)
                    a = angle(m_p[0],m_p[1])
                    #print("angle : ",a)
                    on_action_obj = None
                    for r in level.ressources_instances :
                        if isSelected(r.x + total_delta_x,r.y + total_delta_y,64,64,event.pos[0],event.pos[1]):
                            on_action_obj = r
                            #print("On obj")

        # Gestion des actions
        if not(action_done) and dict_input["MOUSE_CLICK"] :
            print("ACTION : " + str(on_action_obj))
            if on_action_obj != None :  
                on_action_obj.beenAttacked(player.get_damage(on_action_obj.x, on_action_obj.y, total_delta_x, total_delta_y))
                if on_action_obj.life == 0 :
                    level.kill(on_action_obj,list_collision,list_items)
                action_done = True

        # Gestion des deplacement + vitesse
        if dict_input[pygame.K_w] :
            delta_y = -pix_mov*timedelta
        if dict_input[pygame.K_s] :
            delta_y = +pix_mov*timedelta
        if dict_input[pygame.K_d]:
            delta_x = +pix_mov*timedelta
        if dict_input[pygame.K_a]:
            delta_x = -pix_mov*timedelta

        if dict_input[pygame.K_w] and dict_input[pygame.K_d]:
            delta_x = +pix_mov*timedelta*coef_diag
            delta_y = -pix_mov*timedelta*coef_diag
        if dict_input[pygame.K_w] and dict_input[pygame.K_a]:
            delta_x = -pix_mov*timedelta*coef_diag
            delta_y = -pix_mov*timedelta*coef_diag
        if dict_input[pygame.K_s] and dict_input[pygame.K_d]:
            delta_x = +pix_mov*timedelta*coef_diag
            delta_y = +pix_mov*timedelta*coef_diag
        if dict_input[pygame.K_s] and dict_input[pygame.K_a]:
            delta_x = -pix_mov*timedelta*coef_diag
            delta_y = +pix_mov*timedelta*coef_diag

        
        # Control des collisions
        new_list_collision = list_collision_add_delta(list_collision,total_delta_x,total_delta_y)
        if not(chevoch_list(player.hitbox(delta_x,delta_y),new_list_collision)):
            total_delta_x -= delta_x
            total_delta_y -= delta_y

        # Control des items
        for i in list_items :
            if chevoch(player.hitbox(delta_x,delta_y),(i.x + total_delta_x ,i.y + total_delta_y,(16,16))):
                player.inventory_add(i)
                print(player.inventory)
                list_items.remove(i)

        # MOVE HEN
        for hen in hen_list:
            hen.move(total_delta_x, total_delta_y,list_collision,timedelta)

        # BACKGROUND
        screen.blit(background, (0, 0))

        # MAP
        game_map.printMap(screen, total_delta_x, total_delta_y)
        game_map.incIndex(time_cpt)

        # RESSOURCES
        for r in level.ressources_instances :
            r.print(screen,total_delta_x,total_delta_y,time_cpt)

        # ENNEMIES
        screen.blit(slime.currentImg(time_cpt%30 == 0), (slime.x + total_delta_x, slime.y + total_delta_y))

        # HEN
        for hen in hen_list:
            hen.print(screen,total_delta_x, total_delta_y,time_cpt)

        # ITEMS ON FLOOR
        for i in list_items :
            screen.blit(i.currentImg(), (i.x + total_delta_x, i.y + total_delta_y))

        # PLAYER
        #screen.blit(player.currentImg(time_cpt%30 == 0), (player.x, player.y))
        player.print(screen, time_cpt, mouse_position)

        # RESSOURCES LAYER 2
        for r in level.ressources_instances :
            r.print_layer2(screen,total_delta_x,total_delta_y,time_cpt)

        # UI
        ui.print(screen, font_items)

        # APPLY CHANGE
        pygame.display.update()
        
    pygame.quit()
        
        
 
if __name__ == '__main__': main()