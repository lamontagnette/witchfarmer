from math import acos, pi

def chevoch(A,B):
    # (x,y,(x_len,y_len))
    x_ok = abs(A[0]+A[2][0]/2-B[0]-B[2][0]/2) <= A[2][0]/2+B[2][0]/2
    y_ok = abs(A[1]+A[2][1]/2-B[1]-B[2][1]/2) <= A[2][1]/2+B[2][1]/2
    return x_ok and y_ok

def chevoch_list(obj,l):
    for o in l :
        if chevoch(obj,o) :
            return True
    return False

def isSelected(x,y,x_l,y_l,x_mouse,y_mouse) :
    return x_mouse > x and x_mouse < x + x_l and y_mouse > y and y_mouse < y + y_l

def list_collision_add_delta(l, delta_x, delta_y):
    new_l = []
    for e in l :
        new_l.append((e[0] + delta_x, e[1] + delta_y,(e[2][0], e[2][1])))

    return new_l

def angle(x,y):
    a = acos(x)
    if y < 0 :
        return a * 360 / (2*pi)
    else :
        return (2*pi - a) * 360 / (2*pi)