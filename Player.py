import pygame
from utils import *

class Player:

    i_images = [pygame.image.load("images/players/maitre_sorcier.png"),pygame.image.load("images/players/maitre_sorcier_2.png")]
    m_images = [pygame.image.load("images/players/maitre_sorcier.png")]

    hand_position = [(22,26),(22,22)]

    def __init__(self,x,y,weapon):
        self.x = x
        self.y = y
        self.inventory_capability = 10
        self.inventory_max_in_one = 5
        self.inventory = [["",0] for _ in range(self.inventory_capability)]
        self.state = "I"
        self.index_img = 0
        self.weapon = weapon

    def move(self,deltaX,deltaY):
        self.x += deltaX
        self.y += deltaY
    
    def print(self, screen, time_cpt, mouse_position):
        screen.blit(self.currentImg(time_cpt%30 == 0), (self.x, self.y))
        self.weapon.print(screen,time_cpt,self.x,self.y,self.hand_position[self.index_img], mouse_position)

    def currentImg(self, change):
        if self.state == "I" :
            if change :
                self.index_img = (self.index_img+1)%len(self.i_images)
            return self.i_images[self.index_img]

    def hitbox(self, delta_x = 0, delta_y = 0):
        return (self.x + delta_x + 14, self.y + delta_y + 38,(32,22))

    def mvHitbox(self, delta_x, delta_y):
        # TODO : DELETE
        h = self.hitbox()
        return (h[0] + delta_x, h[1] + delta_y, h[2])

    def inventory_add(self,item):
        """
        if i.type in self.inventory :
            self.inventory[i.type] += 1
        else :
            self.inventory[i.type] = 1
        """
        for i in range(self.inventory_capability):
            if self.inventory[i][0] == item.type and self.inventory[i][1] < self.inventory_max_in_one:
                self.inventory[i][1] += 1
                break
            elif self.inventory[i][0] == "":
                self.inventory[i][0] = item.type
                self.inventory[i][1] = 1
                break

    def inventory_verify(self,item):
        nb_item_box = 0
        for i in self.inventory :
            nb_item_box += self.inventory[i]//5 + 1
        if nb_item_box <= self.inventory_capability :
            if item in self.inventory :
                return True
            else :
                return False
        else :
            return True

    def get_damage(self, x, y,total_delta_x,total_delta_y):
        return self.weapon.get_damage(x + total_delta_x, y + total_delta_y, self.x, self.y)