import pygame
from random import randint
from utils import chevoch_list
from cte import *

class Animal :

    animals_images = {
        "baby_hen" : {
            "I" : [
                pygame.image.load("images/animals/hen/baby_hen_d.png"),
                pygame.image.load("images/animals/hen/baby_hen_g.png")
            ],
            "WD" : [
                pygame.image.load("images/animals/hen/baby_hen_d_walk.png"),
                pygame.image.load("images/animals/hen/baby_hen_d_walk2.png")
            ],
            "WG" : [
                pygame.image.load("images/animals/hen/baby_hen_g_walk.png"),
                pygame.image.load("images/animals/hen/baby_hen_g_walk2.png")
            ]
        },
        "egg" : {
            "I" : [
                pygame.image.load("images/animals/hen/egg.png")
            ] 
        }
    }

    def __init__(self,x,y,a_type):
        self.x = x
        self.y = y
        self.type = a_type
        
        if a_type == "egg" :
            self.baby_timer = 60 * 5
            self.state = 'I'
        else :
            self.baby_timer = 0
            self.state = 'I'

        self.index = 0
        self.direction = (0,0)


    def move(self,total_delta_x, total_delta_y,list_collision,timedelta):
        if self.type == "baby_hen" :
            if self.state == "I" :
                if randint(0,100) < 6 :

                    delta_x = randint(0,2) - 1 
                    delta_y = randint(0,2) - 1
                    while (delta_x,delta_y) == (0,0):
                        delta_x = randint(0,2) - 1 
                        delta_y = randint(0,2) - 1
                        
                    self.direction = (delta_x,delta_y)
                    if delta_x == 1 :
                        self.state = "WD"
                    else :
                        self.state = "WG"
                    
            elif self.state == "WD" or self.state == "WG" :
                
                if randint(0,100) > 2 and not(chevoch_list((self.x + self.direction[0]*pix_mov_baby_hen*timedelta - 10, self.y + self.direction[1]*pix_mov_baby_hen*timedelta,(32,32)), list_collision)):
                    if (self.direction[0] == 1 or self.direction[0] == -1) and (self.direction[1] == 1 or self.direction[1] == -1) :
                        self.x += self.direction[0]*pix_mov_baby_hen*timedelta*coef_diag
                        self.y += self.direction[1]*pix_mov_baby_hen*timedelta*coef_diag
                    else :
                        self.x += self.direction[0]*pix_mov_baby_hen*timedelta
                        self.y += self.direction[1]*pix_mov_baby_hen*timedelta
                
                else :
                    self.state = "I"
                    self.direction = (0,0)

        
     
    def print(self,screen,total_delta_x, total_delta_y,cpt) :
        
        if self.baby_timer == 0 :
            if self.type == "egg" :
                self.type = "baby_hen"
        else :
            self.baby_timer -= 1

        if self.x + total_delta_x < SCREEN_SIZE[0] and self.x + total_delta_x > -64 and self.y + total_delta_y < SCREEN_SIZE[1] + 64 and self.y + total_delta_y > -64 :
            if self.type == "egg" :
                screen.blit(self.animals_images[self.type][self.state][0], (self.x + total_delta_x, self.y + total_delta_y))
            elif self.type == "baby_hen" :
                screen.blit(self.animals_images[self.type][self.state][self.index], (self.x + total_delta_x, self.y + total_delta_y))

            if cpt%30 == 0 :
                self.index = (self.index+1)%len(self.animals_images[self.type][self.state])
    
