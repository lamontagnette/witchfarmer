import pygame
from cte import *

class Ui:

    items_bar_image = pygame.image.load("images/ui/items_bar.png")
    items_bar_selected_image = pygame.image.load("images/ui/items_bar_selected.png")
    bar_len = (424,41)

    item_images = {
        "fruit_rouge":pygame.image.load("images/items/ui/fruit_rouge.png"),
        "item_stone":pygame.image.load("images/items/ui/item_stone.png"),
        "item_wood":pygame.image.load("images/items/ui/item_wood.png")
    }

    def __init__(self,player): 
        self.player = player
        self.x_bar = SCREEN_SIZE[0] // 2 - self.bar_len[0] // 2
        self.y_bar = SCREEN_SIZE[1] - self.bar_len[1]
        self.selected = 0
        self.max = 10

    def change_selected(self,n):
        if self.selected + n < 0 :
            self.selected = self.max -1
        elif self.selected + n > self.max - 1 :
            self.selected = 0
        else :
            self.selected += n

    def print(self,screen,font):
        screen.blit(self.items_bar_image, (self.x_bar, self.y_bar))
        screen.blit(self.items_bar_selected_image, (self.x_bar + 3 + self.selected*42, self.y_bar + 2))
        for i in range(self.max):
            if self.player.inventory[i][0] != "":
                screen.blit(self.item_images[self.player.inventory[i][0]], (self.x_bar + 8 + i*42, self.y_bar + 6))
 
                green = (0, 255, 0) 
                black = (0, 0, 0) 
                text = font.render(str(self.player.inventory[i][1]), True, green, black) 
                screen.blit(text,(self.x_bar + 5 + i*42, self.y_bar + 4) )
            else:
                break
            