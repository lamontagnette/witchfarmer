import pygame
from math import sqrt
from utils import angle
from cte import SCREEN_SIZE

class Weapon:

    weapon_image = {
        "basic"  : [
        (0,pygame.image.load("images/weapon/basic_0.png")),
        (45,pygame.image.load("images/weapon/basic_45.png")),
        (90,pygame.image.load("images/weapon/basic_90.png")),
        (135,pygame.image.load("images/weapon/basic_135.png")),
        (180,pygame.image.load("images/weapon/basic_180.png")),
        (255,pygame.image.load("images/weapon/basic_225.png")),
        (270,pygame.image.load("images/weapon/basic_270.png")),
        (315,pygame.image.load("images/weapon/basic_315.png"))
        ]
    }

    weapon_info = {
        "basic" : (20,100) # damage, range
    }

    def __init__(self,r_type):
        self.type = r_type
        self.rotation = 0
        self.index_img = 0
        self.state = 'I'

    def print(self,screen,time_cpt,x,y,hand_position,mouse_position):
        m_p = (mouse_position[0] - SCREEN_SIZE[0]//2, mouse_position[1] - SCREEN_SIZE[1]//2 )
        d = sqrt(m_p[0]**2 + m_p[1]**2) + 0.01
        m_p = (m_p[0]/d,m_p[1]/d)
        a = angle(m_p[0],m_p[1])

        #a = angle( (mouse_position[0] - SCREEN_SIZE[0]//2)/(SCREEN_SIZE[0]//2), (mouse_position[1] - SCREEN_SIZE[1]//2)/(SCREEN_SIZE[1]//2))
        #print("angle : ",a)
        img = self.nearestAngle(a)[1]
        
        screen.blit(img, (x + hand_position[0], y + hand_position[1]))

    def nearestAngle(self,a):
        for i in range(1,len(self.weapon_image[self.type])) :
            if self.weapon_image[self.type][i][0] > a : 
                break

        if i == (len(self.weapon_image[self.type]) - 1) :
            if abs(self.weapon_image[self.type][i][0] - a) < abs(360 - a) :
                return self.weapon_image[self.type][i]
            else :
                return self.weapon_image[self.type][0]
        else : 
            if abs(self.weapon_image[self.type][i][0] - a) < abs(self.weapon_image[self.type][i - 1][0] - a) :
                return self.weapon_image[self.type][i]
            else :
                return self.weapon_image[self.type][i - 1]

    def get_damage(self, x_obj, y_obj, x_player, y_player):
        print("distance : ",sqrt( (x_obj - x_player)**2 + (y_obj - y_player)**2 ))
        if sqrt( (x_obj - x_player)**2 + (y_obj - y_player)**2 ) < self.weapon_info[self.type][1] :
            return self.weapon_info[self.type][0]
        else :
            return 0

