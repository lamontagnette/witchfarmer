import pygame
from cte import *

class Item:
    i_images = {
        "fruit_rouge":[pygame.image.load("images/items/fruit_rouge.png")],
        "item_stone":[pygame.image.load("images/items/item_stone.png")],
        "item_wood":[pygame.image.load("images/items/item_wood.png")]
    }

    def __init__(self,x,y,r_type):
        self.x = x
        self.y = y
        self.type = r_type

    def currentImg(self):
        return self.i_images[self.type][0]

    def print(self,screen,total_delta_x,total_delta_y,time_cpt):
        if self.x + total_delta_x < SCREEN_SIZE[0] and self.x + total_delta_x > -64 and self.y + total_delta_y < SCREEN_SIZE[1] + 64 and self.y + total_delta_y > -64 :
            screen.blit(self.currentImg(), (self.x + total_delta_x, self.y + total_delta_y))
