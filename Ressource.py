import pygame
from random import randint
from cte import *
from Item import * 

class Ressource:
    i_images = {
        "arbuste":[pygame.image.load("images/ressources/arbuste.png")],
        "arbre":[pygame.image.load("images/ressources/tree1.png"),pygame.image.load("images/ressources/tree2.png")],
        "pierre":[pygame.image.load("images/ressources/stone.png")]
    }

    defense_dict = {
        "arbuste":1,
        "pierre":2,
        "arbre":2
    }

    item_dict = {
        "arbuste":"fruit_rouge",
        "pierre":"item_stone",
        "arbre":"item_wood"
    }

    def __init__(self,x,y,r_type,id_obj):
        self.id = id_obj
        self.x = x
        self.y = y
        self.type = r_type
        self.life = 100
        self.defense = 1
        self.index_img = 0
        self.hitbox = (x,y,(64,64))
        if self.type in ["arbre"] :
            self.layer = 2
        else :
            self.layer = 1

    def beenAttacked(self, attack):
        dammage = attack//self.defense 
        if dammage >= self.life :
            self.life = 0
            #self.beenKilled()
        else :
            self.life -= dammage//self.defense_dict[self.type]
        print("Vie : ",self.life)

    def beenKilled(self, list_items):
        zone = 50
        items_number = 3
        for i in range(0,items_number):
            list_items.append(Item( self.x + randint( i*zone//items_number , (i+1)*zone//items_number - 10), self.y + randint( 0, zone), self.item_dict[self.type]))

        # TODO : killed animation
        print("killed")

    def print(self,screen,total_delta_x,total_delta_y,time_cpt):
        if self.x + total_delta_x < SCREEN_SIZE[0] and self.x + total_delta_x > -64 and self.y + total_delta_y < SCREEN_SIZE[1] + 64 and self.y + total_delta_y > -64 :
            screen.blit(self.i_images[self.type][0], (self.x + total_delta_x, self.y + total_delta_y))

    def print_layer2(self,screen,total_delta_x,total_delta_y,time_cpt):
        if self.layer == 2 :
            if self.x + total_delta_x < SCREEN_SIZE[0] and self.x + total_delta_x > -64 and self.y + total_delta_y < SCREEN_SIZE[1] + 64 and self.y + total_delta_y > -64 :
                screen.blit(self.i_images[self.type][1], (self.x + total_delta_x, self.y - 64 + total_delta_y))