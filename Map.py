import pygame
from random import randint
from cte import *
import Item

class Map :

    dict_block = {
        #'e' : [pygame.image.load("images/map/eau.png")],
        'ed' : [pygame.image.load("images/map/eaudessous.png"),pygame.image.load("images/map/eaudessous2.png"),pygame.image.load("images/map/eaudessous3.png"),pygame.image.load("images/map/eaudessous4.png"),pygame.image.load("images/map/eaudessous3.png"),pygame.image.load("images/map/eaudessous2.png")],
        'p' : [pygame.image.load("images/map/prairie.png")]
    }

    index_img = 0

    def __init__(self,size):
        self.size = size
        self.bitmap = []

        self.params = {'island_layer1' : 3, 'center_noise' : 2} # 3 layer : ()

        for x in range(size[0]):
            self.bitmap.append([])

            for y in range(size[1]):

                self.bitmap[x].append('e')

                if y == 0 :
                    self.bitmap[x][y] = 'e'
                if x == 0 or x == size[0] - 1 or y == size[1] -1 :
                    if self.bitmap[x][y-1] == 'p':
                        self.bitmap[x][y] = 'ed'
                    else :
                        self.bitmap[x][y] = 'e'

                elif x <= self.params['island_layer1'] or y <= self.params['island_layer1'] or x >= self.size[0] - 1 - self.params['island_layer1'] or y >= self.size[1] - 1 - self.params['island_layer1'] :
                    if randint(1,100) <= 50 :
                        if self.bitmap[x][y-1] == 'p':
                            self.bitmap[x][y] = 'ed'
                        else :
                            self.bitmap[x][y] = 'e'
                    else :
                        self.bitmap[x][y] = 'p'
                else :
                    if randint(1,100) <= 5 :
                        if self.bitmap[x][y-1] == 'p':
                            self.bitmap[x][y] = 'ed'
                        else :
                            self.bitmap[x][y] = 'e'
                    else :
                        self.bitmap[x][y] = 'p'

    def printMap(self,screen, delta_x, delta_y):
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                if self.bitmap[i][j] != 'e' and i*64 + delta_x < SCREEN_SIZE[0] + 64 and i*64 + delta_x > -64 and j*64 + delta_y < SCREEN_SIZE[1] + 64 and j*64 + delta_y > -64 :
                    screen.blit(self.dict_block[self.bitmap[i][j]][self.index_img%len(self.dict_block[self.bitmap[i][j]])], (i*64 + delta_x, j*64 + delta_y))
    
    def incIndex(self, time_cpt):
        if time_cpt%10 == 0 :
            self.index_img += 1
            #if self.index_img%2 == 0:
            #    self.index_img = 0

    

if __name__ == '__main__': 
    m = Map((15,15))
    print(m.bitmap)