import pygame

class Ennemie:
    i_images = [pygame.image.load("images/ennemies/slime1.png"),pygame.image.load("images/ennemies/slime2.png")]
    m_images = [pygame.image.load("images/ennemies/slime1.png")]

    def __init__(self,x,y):
        self.x = x
        self.y = y
        self.state = "I"
        self.index_img = 0

    def move(self,deltaX,deltaY):
        self.x += deltaX
        self.y += deltaY

    def currentImg(self, change):
        if self.state == "I" :
            if change :
                self.index_img = (self.index_img+1)%len(self.i_images)
            return self.i_images[self.index_img]