import pygame
from Ressource import *
from random import randint
from utils import chevoch_list
from cte import *

class Level :

    def __init__(self,no,ressources):
        self.no = no
        self.ressources = ressources
        self.ressources_instances = []

    def init_res_instances(self,map_size,list_collision): 
        self.ressources_instances = []

        index = 0
        for r in self.ressources :
            for _ in range(self.ressources[r]):
                x = (randint(64,map_size[0]*64-64)//64)*64
                y = (randint(64,map_size[1]*64-64)//64)*64
                while chevoch_list((x,y,(64,64)),list_collision):
                    x = (randint(64,map_size[0]*64-64)//64)*64
                    y = (randint(64,map_size[1]*64-64)//64)*64
                r_obj = Ressource(x,y,r, index)
                self.ressources_instances.append(r_obj)
                list_collision.append(r_obj.hitbox)
                index += 1
                        
                
        return list_collision

    def kill(self,r,list_collision,list_items):
        #self.ressources_instances.pop(index)
        try :
            self.ressources_instances.remove(r) # put this line first
            list_collision.remove(r.hitbox)
            r.beenKilled(list_items)
        except :
            print("Error : remove instance or collision")
        print("Rm from instances")


